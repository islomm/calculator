import { useState } from "react";
import Button from "../src/component/Button";
import Input from "./component/Input";
import { evaluate } from "mathjs";
import * as math from "mathjs";

function App() {
  const [text, setText] = useState("");
  const [result, setResult] = useState("");
  const addToText = (val) => {
    setText((text) => [...text, val + ""]);
  };
  const calculateResult = () => {
    const input = text.join("");
    setResult(math.evaluate(input));
  };
  const Clear = () => {
    setText("");
    setResult("");
  };
  return (
    <div className="container md:p-5 sm:p-1 md:px-auto sm:px-5 py-5">
      <p className="display-2 fw-bold text-center text-white py-5">
        Calculator !
      </p>
      <Input text={text} result={result} />
      <div className="sm:w-full md:w-1/2 md:mx-auto sm:mx-0 flex border flex-wrap md:gap-3 gap-3 ps-[22px] rounded-b-[20px]">
        <Button num="7" handleClick={addToText} />
        <Button num="8" handleClick={addToText} />
        <Button num="9" handleClick={addToText} />
        <button
          onClick={() => addToText("/")}
          className="bg-orange-400 md:w-1/5 flex items-center sm:w-[25%] px-[30px] h-[80px] my-2 py-[30px] text-2xl font-bold text-white border border-white hover:bg-orange-300 rounded-[25px]"
        >
          /
        </button>

        {/* das */}
        <Button num="4" handleClick={addToText} />
        <Button num="5" handleClick={addToText} />
        <Button num="6" handleClick={addToText} />
        <button
          onClick={() => addToText("*")}
          className="bg-orange-400 md:w-1/5 flex items-center sm:w-[25%] px-[30px] h-[80px] my-2 py-[30px] text-2xl font-bold text-white border border-white hover:bg-orange-300 rounded-[25px]"
        >
          x
        </button>
        {/* das */}
        <Button num="1" handleClick={addToText} />
        <Button num="2" handleClick={addToText} />
        <Button num="3" handleClick={addToText} />
        <button
          onClick={() => addToText("+")}
          className="bg-orange-400 md:w-1/5 flex items-center sm:w-[25%] px-[28px] h-[80px] my-2 py-[30px] text-2xl font-bold text-white border border-white hover:bg-orange-300 rounded-[25px]"
        >
          +
        </button>
        {/* das */}
        <Button num={0} handleClick={addToText} />
        <Button num="." handleClick={addToText} />
        <button
          onClick={calculateResult}
          className="md:w-1/5 sm:w-[25%] flex items-center px-[30px] h-[80px] my-2  border bg-gray-400 text-white font-bold text-lg hover:bg-gray-400 rounded-[25px]"
        >
          =
        </button>
        <button
          onClick={() => addToText("-")}
          className="bg-orange-400 flex items-center md:w-1/5 sm:w-[25%] px-[32px] h-[80px] my-2 py-[30px] text-2xl font-bold text-white border border-white hover:bg-orange-300 rounded-[25px]"
        >
          -
        </button>
        <button
          onClick={Clear}
          className="bg-red-500 w-full p-3 mb-2 text-2xl font-bold text-white hover:bg-red-700 rounded-[30px] me-4"
        >
          Clear
        </button>
      </div>
    </div>
  );
}

export default App;
