import React from "react";

const Button = ({ num, handleClick }) => {
  return (
    <div
      onClick={() => handleClick(num)}
      className="md:w-1/5 sm:w-[25%] px-[30px] h-[80px]  my-2 flex items-center  border bg-gray-400 text-white font-bold text-lg hover:bg-gray-400 rounded-[25px] cursor-pointer"
    >
      <p>{num}</p>
    </div>
  );
};

export default Button;
