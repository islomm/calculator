import React from "react";

const Input = ({ result, text }) => {
  return (
    <div className="input-wrapper items-end bg-white w-full px-4 py-2 h-[150px] rounded-t-lg">
      <div className="result text-end">
        <p className="text-[30px] font-bold">{result}</p>
      </div>
      <div className="text text-end">
        <p className="text-[25px] font-bold">{text}</p>
      </div>
    </div>
  );
};

export default Input;
